// [rule: 番号 ? ]

var flag = param(1).replace("-", "");

var options = {
    "method": "get",
    "url": "https://www.141jav.com/search/?q=" + flag,
    "headers": {
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36 Edg/96.0.1054.62"
    }
}

request(options, (error, response, body) => {
    if ( error || response.statusCode !== 200) {
        return
    }
    body = body.replace(/\n/g, "")
    // 封面图
    var cover = body.match(/<img class="image" src="(.*?)"/)[1]
    // 上映时间
    var OrderTime = body.match(/<p class="subtitle is-6">(.*?)<\/a>/)[1].split(">")[1]
    // 内容简介
    var desp = body.match(/<p class="level has-text-grey-dark">(.*?)<\/p>/)[1]
    // 下载链接
    var magnet = "magnet" + body.match(/href="magnet(.*?)"/)[1]
    sendText("作品番号：" + flag + "\n" + "上映时间：" + OrderTime + "\n" + "内容简介" + desp + "\n" + "下载链接" + magnet + "\n" + "作品封面" + image(cover))
    
})